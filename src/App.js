import React from 'react'

import Dummy from './components/Dummy'

function App() {
  return (
    <div className="App">
      <Dummy />
    </div>
  );
}

export default App;
